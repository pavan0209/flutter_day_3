import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignment1State();
}

class _Assignment1State extends State<Assignment1> {
  final List<int> numList1 = [121, 100, 12321, 1234321, 5454];
  final List<int> numList2 = [153, 1634, 371, 54456, 123];
  final List<int> numList3 = [1, 145, 234, 40585];
  int? _palindromeCnt = 0;
  int? _armStrongCnt = 0;
  int? _strongCnt = 0;

  void showPalindromeCount() {
    setState(() {
      if (_palindromeCnt! > 0) _palindromeCnt = 0;
      for (int num in numList1) {
        int rev = 0;

        for (int i = num; i != 0; i ~/= 10) {
          rev = rev * 10 + i % 10;
        }

        if (rev == num) _palindromeCnt = _palindromeCnt! + 1;
      }
    });
  }

  void showArmStrongCount() {
    setState(() {
      if (_armStrongCnt! > 0) _armStrongCnt = 0;

      for (int num in numList2) {
        int sum = 0;
        int digitCnt = 0;

        for (int i = num; i != 0; i ~/= 10) {
          digitCnt++;
        }

        for (int i = num; i != 0; i ~/= 10) {
          int prod = 1;
          for (int j = 1; j <= digitCnt; j++) {
            prod *= (i % 10);
          }
          sum += prod;
        }

        if (sum == num) _armStrongCnt = _armStrongCnt! + 1;
      }
    });
  }

  void showStrongCount() {
    setState(() {
      if (_strongCnt! > 0) _strongCnt = 0;
      for (int num in numList3) {
        int sum = 0;
        for (int i = num; i != 0; i ~/= 10) {
          int fact = 1;
          for (int j = 2; j <= i % 10; j++) {
            fact *= j;
          }
          sum += fact;
        }

        if (sum == num) _strongCnt = _strongCnt! + 1;
      }
    });
  }

  @override
  Widget build(BuildContext con) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(242, 242, 247, 0.6),
      appBar: AppBar(
        title: const Text(
          "Palindrome Number or not",
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "List for checking palindrome numbers :: $numList1",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: showPalindromeCount,
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.cyan.shade300,
                    foregroundColor: Colors.black,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 15,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  child: const Text(
                    "Check Palindrome",
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
                Text(
                  "$_palindromeCnt",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "List for checking armstrong numbers :: $numList2",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
            const SizedBox(
              width: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: showArmStrongCount,
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.cyan.shade300,
                    foregroundColor: Colors.black,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 15,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  child: const Text(
                    "Check ArmStrong",
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
                Text(
                  "$_armStrongCnt",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "List for checking strong numbers :: $numList3",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
            const SizedBox(
              width: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: showStrongCount,
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.cyan.shade300,
                    foregroundColor: Colors.black,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 15,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  child: const Text(
                    "Check Strong",
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
                Text(
                  "$_strongCnt",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
